---
title: "Go Tutorial"
date: 2020-11-29T22:08:00+05:30
lastmod: 2020-11-29T22:08:00+05:30
tags : [ "go", "tutorial"]
categories : [ "go" ]
layout: post
type:  "post"
highlight: false
draft: false
---

![Text](https://miro.medium.com/max/1400/1*Ifpd_HtDiK9u6h68SZgNuA.png "Go")


### Variables
Variables, if declared always have to be used. Compiler will throw an error if you declare a variable and not use it.  

**Variable Declaration**  
Following format is used when you are want to declare a variable but not ready to initialize it yet.
```
var i int
i = 42
```

Next syntax is valuable if go doesn't have enough information to deduce the type of the variable.
```
var i int = 42

// For example, if we do
i := 99  
fmt.Println("%v, %T",i,i)               Output: 99, int

i := 99.
fmt.Println("%v, %T",i,i)               Output: 99, float64
```
There is no way to have float32 type if we don't specify. Above syntax is useful in such situation.
```
var i float32 = 99
fmt.Println("%v, %T",i,i)               Output: 99, float32
```    
This method is also used for declaring many variables at once
```
var (
    employeeName string = "Jack"
    spouseName string = "Jones"
    bookingNumber int = 102
)
```
Third method is used for declaring and initializing a variable at once.
It is a shorthand version for declaring variable and let the compiler decide the type based on the value assigned.  
```
i := 42
```
**Redeclaration and Shadowing**  
Go doesn't allow to redeclare variables in the same scope. But they can be declared in separate scope called Shadowing.
```
var i int = 27

func main() {
    fmt.Println(i)                      Output: 27   
    var i int = 42            //SHADOWING Variable
    fmt.Println(i)                      Output: 42     
}
```
All variables must be used in Go. You can't declare a variable and not use it.

**Visibility and Naming Conventions**    
Naming controls the visibility of variables in Golang. CamelCase convention is used for naming variables.
```
//Lowercase first letter are for Package level scope. All files in the same package can access it.
var i int = 42

//Uppercase first letter exposes it globally.
var I int = 42 
```
Length of the variable name should indicate the lifespan of the variable.
You can use single letter variable for block scope variable.  
  

Keep acronoyms as Uppercase
```
theURL := "https://abc.xyz"
```
**Type Conversions**  
```
var i int = 42
var j float32;  
j=float32(i)
```
Go will give compile time error for lossy conversion.
Go doesn't do implicit type conversion, you have to do the conversion.
```
int to string conversion  
i := 42
string(i)               '*'         //prints the ASCII
strconv.Itoa(i)         '42'        //need to use the strconv package 
```  
  
### Primitives  
Everytime you declare a variable and not initialize, it has a zero value.  

**Boolean** 
Boolean variable represent two states true and false.
They are mainly used as state flags and logical expressions. 
```
var n bool = true
fmt.Println("%v , %T\n",n,n)                       false, bool
```
Zero value for boolean is false.  

**Numeric**  
Zero Value for Numeric type is zero.  
**1. Integer**
```  
int8      -128 to 127   
int16     -32768 to 32767  
int32     -2*10^9 to 2*10^9   
int64     -9*10^18 to 9*10^18    
```
There are equivalent type of unsigned integer for signed integer except int64. 
We are not allowed to add two integers different type.

```
var a int = 10
var b int8 = 3
fmt.Println(a+b)                   //Error

fmt.Println(a+int(b))              Output: 13 
```  
Even the both the int types are almost equivalent, Go is not going to make any assumption, hence will not do any type conversion. Go doesn't do any implicit type conversion. can't mix type in the same family.(uint16 + uint32 = error) 

int is either 32 or 64 bits, depending on the implementation. Usually it's 32 bits for 32-bit compilers and 64 bits for 64-bit compilers.  

byte is a type alias for uint8. 
var a string = "This is a string"
runes are just a type alias for int32.
r := 'a'

### Constants

You can't assign anything to const variable at runtime during initialization.  
const myConst float64 = math.Sin(1.57)  
 