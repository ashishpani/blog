---
title: "Archetypes"
date: 2020-11-26T03:12:46+05:30
lastmod: 2020-11-26T03:12:46+05:30
tags : ["hugo", "template"]
categories : [ "dev" ]
layout: post
type:  "post"
highlight: false
draft: false
---

### Archetypes

<!-- [Text](https://www.gohugo.io "Title") -->
![Text](https://d33wubrfki0l68.cloudfront.net/c38c7334cc3f23585738e40334284fddcaf03d5e/2e17c/images/hugo-logo-wide.svg "Title")
Hugo uses archetypes to define this default content template. The archetypes directory is where you place Markdown templates for various types of content. An “archetype” is an original model or pattern that you use as the basis for other things of the same type. Hugo uses the files in the archetypes folder as models when it generates new content pages. There’s a default one that places a title and date in the file and sets the draft status to true. You’ll create new ones later.

### Creating Content Using Archetypes

You can tell Hugo to create content pages that contain placeholder content. This way, you never have to start with a blank slate when creating content. Hugo uses archetypes to define this default content. The hugo new command uses the default.md file as a template to create a new Markdown file. 

### Generating Final Content

Execute this command to generate the deployable.
```
​$ ​​hugo​​ ​​--cleanDestinationDir​​ ​​--minify
```