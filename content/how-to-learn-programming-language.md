---
title: "How to learn a Programming Language quickly?"
date: 2021-01-03T03:07:19+05:30
lastmod: 2021-01-03T03:07:19+05:30
tags : [ "dev", "learn"]
categories : [ "dev" ]
layout: post
type:  "post"
highlight: false
draft: false
---

Following would be my cheklist  

## Basic
This is all about focusing on just fundamental programming concepts and really understanding the basic building blocks of programming in that language. 
Understand the core syntax, the way different lines of code are going to be executed, conditions and variable declaration. 

1. How variables work?
82. Types of variables
2. Booleans and comparision of variables
3. Conditions, else chaining and chaining conditions
4. Evaluating multiple conditions
5. Check for the supported operators
6. Data Structures
     1. Lists
     3. Sets
     2. Dictionary
99. Control flow
7. Loops and iterables
    1. How to loop through lists and dictionary?
    2. Looping constructs

8. Functions
9. Mutable and Immutable Data types  

10. How the data is passed to the function?  
Pass a list to the function and modify that list (In most languages, its kind of in-place rather than creating a new version of it). Whereas with a string, I cannot modify the original string that i pass into the function. Try all this.

11. Common methods related to string and list (string and list operations)

12. IO operation such as Input and Output (Read and write from stdout or file)
Try out things you may need to do if you want to make some king of real applications.  


Its very important to practice the skills you are learning. Need to implement something on your own.
You should not be watching videos that tell you about these things.
**Project:** TicTacToe in commandline  

## Intermediate

1. Object Oriented Programming  
2. Understanding Classes  
3. Design Patterns
4. Good practices with OOP
5. Try solving some algo questions with the language
6. Learn HashMaps, Stacks, Priority Queue
7. Learn about language Idioms, One liners
8. Anonymous function (lambda)
9. Map, Reduce and Filter operations
10. Variable arguments
11. Advance class behaviour like static things or operator overiding or overriding default behaviour
12. Language Package manager
13. Making Modules, separating your code or environments


## Advanced
Learn it when you need to learn it kind of stuff.
8. Logging and Debugging
1. Async behaviour
2. Decorators
3. Generators
4. Concurrency and Parallelism. How the language supports it?
5. Unit Testing
6. Understand the test pipeline
7. How to build packages?


## Master/Expert

Focus on the specific areas and other packages available.
Make some projects and apply the skills you learnt.
